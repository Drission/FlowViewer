request 监听结果保存为`RequestData`对象，response 监听结果保存为`ResponseData`对象。

# 🔧`RequestData`类

属性：

- requestId：浏览器保存的请求唯一 id

- fetchId：请求在浏览器 Fetch 协议下的 id

- request：requst 数据，是一个大小写不敏感的字典

- headers：headers 数据，是一个大小写不敏感的字典

- tab：产生这个请求的标签页的 id

- target：产生这个请求的监听目标

除以上属性，`RequestData`可通过 key 或指定属性来访问具体的 request 字段：

```python
# 假设rd是一个RequestData对象
print(rd.headers['referer'])  # 打印request中的referer数据
print(rd['headers']['referer'])  # 和上面一行一致
```

其它可以使用的属性有：

url、urlFragment、method、postData、hasPostData、postDataEntries、mixedContentType、initialPriority、referrerPolicy、isLinkPreload、rustTokenParams、isSameSite

# 🔧`ResponseData`类

属性：

- requestId：浏览器保存的请求唯一 id

- response：response 数据，是一个大小写不敏感的字典

- body：reponse body 数据，如果请求是 json 格式，返回转换后的字典，如果是图片，进行 base64 转码后返回字节，否则返回其文本

- rawBody：转换字典前的 body 原始文本

- postData：如果是 post 方式请求，返回其 post data 文本，否则为`None`（Listener不一定包含此数据，可尝试RequestMan）

- headers：以大小写不敏感字典方式返回 response 的 headers

- requestHeaders：以大小写不敏感字典方式返回 request 的 headers

- tab：产生这个请求的标签页的 id

- target：产生这个请求的监听目标

除以上属性，`ResponseData`可通过 key 或指定属性来访问具体的 response 字段：

```python
# 假设rd是一个ResponseData对象
print(rd.url)  # 打印response中的url数据
print(rd['url'])  # 和上面一行一致
```

其它可以使用的属性有：

url、status、statusText、headersText、mimeType、requestHeadersText、connectionReused、connectionId、remoteIPAddress、remotePort、fromDiskCache、fromServiceWorker、 fromPrefetchCache、encodedDataLength、timing、serviceWorkerResponseSource、responseTime、 cacheStorageCacheName、protocol、securityState、securityDetails
