`Listener`类用于监听 response 数据包，`RequestMan`类用于监听 request 数据包，两者用法一致，只是监听的对象不同、返回的数据类型不同。这里以`Listener`类作介绍

# 属性

## listening

该属性返回监听器当前是否有在执行监听，为`bool`类型。

## tab_id

该属性返回当前监听的标签页 id，为`str`类型。

## targets

该属性返回当前监听的目标，为`str`或`str`组成的`list`或`tuple`。

## active_tab

该属性返回当前浏览器活动标签页的 id，为`str`类型。

## results

该属性返回监听结果的列表。`Listener`类结果为`ResponseData`类型，`RequestMan`类结果为`RequestData`类型。

## show_msg

该属性用于设置监听开始和结束时是否打印提示，为`bool`类型。

# 方法

## set_target()

该方法用于设置要监听的目标。目标为`str`或`str`组成的`list`或`tuple`。

设置后，当对象执行监听，就会使用这些目标，而不用每次监听单独设置。

设置`True`，则会采集所有请求。

参数：

- `targets`：要设置的目标，可以为多个

返回：`None`

```python
listener.set_targets(True)  # 获取所有请求
listener.set_targets('baidu.com')  # 设置单个目标
listener.set_targets(('baidu.com', '163.com'))  # 设置多个目标
```

## listen()

该方法用于启动监听。可异步或同步进行。

可在启动时设置要监听的对象，如果设置，新设置会修改对象中保存的目标。

可设置监听结果上限和时限，监听到的目标个数到达上限，或到达超时时间，就会停止。

参数：

- `targets`：与`set_targets()`方法一致，设为`None`则使用原有设置

- `count`：要获取目标数据包个数上限

- `timeout`：监听期限，单位为秒

- `asyn`：是否异步监听，默认为`True`

返回：`None`

## steps()

该方法返回一个生成器，用于实时获取监听到的数据包。

遍历这个生成器，每一步为一组监听结果，可设置每一组的数量。

参数：

- `gap`：每组返回数据的个数，默认为 1

返回：数据包对象组成的生成器

## stop()

该方法用于停止监听。

可用于自行判断监听停止的条件。

参数：无

返回：`None`

## wait()

该方法用于阻塞线程等待监听结束。

参数：无

返回：`None`

## to_tab()

该方法用于切换要监听的标签页。

监听的标签页无须处于活动状态。

参数：

- `handle_or_id`：标签页的 handle 值或 id 值，不指定时切换到浏览器当前活动标签页

- `browser`：如要切换浏览器，可指定浏览器端口、url，或传入`MixPage`对象

返回：`None`

## get_results()

该方法用于获取监听结果，可指定要获取的目标，不指定则返回全部。

参数：

- `target`：要获取的目标

返回：由结果对象组成的列表
